import re
import time
import csv
import json
import requests
from fake_useragent import UserAgent

base_url = "http://127.0.0.1:8000/"


class KeywordGetter:
    def __init__(self):
        self.queue = set()
        self.country = "us"
        self.language = "en"
        self.threshold_count = 1000
        self.keywords_count = 0
        self.results = []

    def checkSeedKeywordExists(self, keyword, meta_keyword):
        keyword_ = re.sub('[^A-Za-z0-9]+', '', keyword)
        if seed_keyword in keyword or seed_keyword in keyword_:
            return True
        else:
            return False

    def fetchSuggestion(self, keyword, seed_keyword, meta_keyword):
        """ return list of suggestion based on the geolocation and language for a keyword """
        url = "http://suggestqueries.google.com/complete/search?client=chrome&hl={}&gl={}&callback=?&q={}".format(
            self.language, self.country, keyword)
        ua = UserAgent(use_cache_server=False, verify_ssl=False)
        headers = {"user-agent": ua.chrome, "dataType": "jsonp"}

        response = requests.get(url, headers=headers, verify=True)
        suggestions = json.loads(response.text)

        sugg = []
        index = 0
        relevancies = []
        if "google:suggestrelevance" in suggestions[4].keys():
            relevancies = suggestions[4]['google:suggestrelevance']
        for word in suggestions[1]:
            if self.checkSeedKeywordExists(word, meta_keyword):
                sugg.append({
                    'keyword': word,
                    'relevancy_score': relevancies[index] if len(relevancies) > 0 else None,
                    'seed_keyword': seed_keyword,
                    'meta_keyword': meta_keyword,
                })
            else:
                continue
            index += 1

        return sugg

    def fetchRelatedkeywords(self, keyword, meta_keyword):
        """ return all questions for a keyword with search volumes """
        suffix = ["", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l",
                  "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w" "x", "y", "z"]
        suffix_arr = list(map(lambda x: keyword+" "+x, suffix))
        duplicates = set()
        for word in suffix_arr:
            suggestion = self.fetchSuggestion(word, keyword, meta_keyword)
            for query in suggestion:
                if query['keyword'] not in duplicates:
                    duplicates.add(query['keyword'])
                    self.results.append(query)
                    self.queue.add(query['keyword'])
        self.keywords_count += len(self.results)


def getSeedKeywords():
    res = requests.get('{}seedkeywords/list/'.format(base_url))
    json_res = res.json()
    return json_res


def updatestatus(id, status):
    res = requests.put('{}update/{}/'.format(base_url, id),
                       data={"keyword_fetching_status": status})
    res = res.json()
    return res


def uploadcsvfile(filename):
    files = {'csvfile': open(filename, 'rb')}
    res = requests.post("{}upload/csvfile/".format(base_url), files=files)
    return res.status_code


if __name__ == "__main__":
    keyword_getter = KeywordGetter()
    while(len(getSeedKeywords()) > 0):
        # writes  keywords data into file
        s_keyword = getSeedKeywords()[0]
        meta_keyword = s_keyword['keyword']
        if s_keyword['keyword_fetching_status'] == 1:
            keyword_getter.keywords_count = 0
        ofile = open("{}_keywords.csv".format(meta_keyword), 'w+')
        writer = csv.DictWriter(ofile, fieldnames=[
                                "keyword", "relevancy_score", "seed_keyword", "meta_keyword"])
        writer.writeheader()
        flag = True

        keyword_getter.queue.add(meta_keyword)
        # updates seed keyword fetching status to 1 which is Inprocess
        updatestatus(s_keyword['id'], 1)
        while(flag):
            ofile = open("{}_keywords.csv".format(meta_keyword), 'a')
            writer = csv.DictWriter(ofile, fieldnames=[
                                    "keyword", "relevancy_score", "seed_keyword", "meta_keyword"])
            try:
                seed_keyword = keyword_getter.queue.pop()
                keyword_getter.fetchRelatedkeywords(seed_keyword, meta_keyword)
            except:
                time.sleep(10)
            writer.writerows(keyword_getter.results)
            ofile.close()
            if keyword_getter.keywords_count > keyword_getter.threshold_count:
                flag = False
            keyword_getter.results = []
        keyword_getter.queue = set()
        keyword_getter.keywords_count = 0
        # updates seed keyword fetching status to 2 which is Completed
        updatestatus(s_keyword['id'], 2)
        uploadcsvfile("{}_keywords.csv".format(meta_keyword))
